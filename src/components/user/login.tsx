import React, { useEffect, useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import Toast from 'react-bootstrap/Toast';
import ToastContainer from 'react-bootstrap/ToastContainer';
import { Link, useNavigate } from 'react-router-dom';
import { useUser } from '../../hooks/useUser';

export const Login = () => {
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [notificationMessage, setNotificationMessage] = useState('');
    const [show, setShow] = useState(false);
    const navigate = useNavigate();
    const { isLogged, login, hasLoginError } = useUser();

    useEffect(() => {
        if (isLogged) navigate('/');

    }, [isLogged])

    useEffect(() => {
        if (hasLoginError) {
            setShow(true);
            setNotificationMessage('Usuario o Contraseña no coinciden');
        }
    }, [hasLoginError])

    const closeNotification = () => {
        setShow(false);
        setNotificationMessage('');
    }

    const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setEmail(event.target.value);
    };

    const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(event.target.value);
    };

    const handleSubmit = async () => {
        await login(email, password);
        if (hasLoginError) {
            setShow(true);
            setNotificationMessage('Usuario o Contraseña no coinciden');
        }
    };

    return (
        <>
            <ToastContainer
                className="p-3"
                position='top-center'
                style={{ zIndex: 1 }}
            ><Toast bg='danger' onClose={closeNotification} show={show} delay={3000} autohide>
                    <Toast.Header>
                        <img src="holder.js/20x20?text=%20" className="rounded me-2" alt="" />
                        <strong className="me-auto">Error en la autenticación</strong>
                    </Toast.Header>
                    <Toast.Body>{notificationMessage}</Toast.Body>
                </Toast >
            </ToastContainer>

            <Form className="registration-form">
                <h2>Inicio de sesión</h2>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Ingresa tu email"
                        value={email}
                        onChange={handleEmailChange}
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Contraseña</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Contraseña"
                        value={password}
                        onChange={handlePasswordChange}
                    />
                </Form.Group>
                <Button onClick={handleSubmit}>
                    Iniciar Sesión
                </Button>
                <p>¿No estás registrado aún? <Link to="/signup">Regístrate</Link></p>
            </Form>
        </>
    );
};

export default Login;
