import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import { User } from '../../models/user'
import UserService from '../../services/userService';
import Toast from 'react-bootstrap/Toast';
import ToastContainer from 'react-bootstrap/ToastContainer';
import { Link, useNavigate } from 'react-router-dom';
import { useUser } from '../../hooks/useUser';

export const Signup = () => {
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [notificationMessage, setNotificationMessage] = useState('');
    const [show, setShow] = useState(false);
    const navigate = useNavigate();
    const { login } = useUser();

    const closeNotification = () => {
        setShow(false);
        setNotificationMessage('');

    }

    const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setEmail(event.target.value);
    };

    const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(event.target.value);
    };

    const handleSubmit = async () => {
        const user: User = new User(email, password);
        const rpta = await UserService.create(user as User);
        if (rpta.status != 200) {
            setNotificationMessage(rpta.body.msg);
            setShow(true);
        } else {
            await login(email, password);
            navigate('/');
        }
    };

    return (
        <>
            <ToastContainer
                className="p-3"
                position='top-center'
                style={{ zIndex: 1 }}
            ><Toast bg='danger' onClose={closeNotification} show={show} delay={3000} autohide>
                    <Toast.Header>
                        <img src="holder.js/20x20?text=%20" className="rounded me-2" alt="" />
                        <strong className="me-auto">Error en el registro</strong>
                    </Toast.Header>
                    <Toast.Body>{notificationMessage}</Toast.Body>
                </Toast >
            </ToastContainer>
            <Form className="registration-form">
                <h2>Registro de usuarios</h2>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Ingresa tu email"
                        value={email}
                        onChange={handleEmailChange}
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Contraseña</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Contraseña"
                        value={password}
                        onChange={handlePasswordChange}
                    />
                </Form.Group>
                <Button variant="primary" onClick={handleSubmit}>
                    Registrarse
                </Button>
                <p>¿Ya eres un usuario? Inicia sesión <Link to="/login">aquí</Link></p>
            </Form>
        </>
    );
};

export default Signup;
