import React from 'react';
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import NavDropdown from "react-bootstrap/NavDropdown";
import Nav from "react-bootstrap/Nav";
import { Link, useNavigate } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { useUser } from '../hooks/useUser';

function navBarComponent() {
    const navigate = useNavigate();
    const handleCloseSesion = () => {
        logout();
        navigate('/login');
    }
    const { isLogged, logout } = useUser();
    return (
        <>
            <Navbar bg="light" expand="lg">
                <Container>
                    <Navbar.Brand ><Link to="/" >Proyecto Biblia</Link></Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            {/* <Link to="/" >
                                Proyectos
                            </Link> */}

                            <NavDropdown title="Perfil" id="basic-nav-dropdown">
                                {!isLogged && <NavDropdown.Item >
                                    <Link to="/signup" >
                                        Registrarse
                                    </Link>
                                </NavDropdown.Item>}
                                <NavDropdown.Item >{
                                    isLogged ? <Button onClick={handleCloseSesion} variant="link">
                                        Cerrar Sesión
                                    </Button> : <Link to='/login' >
                                        Iniciar Sesión
                                    </Link>}

                                </NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar >
        </>
    );
}

export default navBarComponent;
