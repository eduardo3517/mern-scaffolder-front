import React, { useState, useEffect } from 'react';
import BookService from "../../services/bookService";
import { Table, Button, Accordion, Card, Spinner, ToastContainer, Toast, Container } from "react-bootstrap";
import { diccionary } from "../../config";
// import NewBookModal from "./newBook";
import model from "../../models/book";
import chapterModel from "../../models/chapter";
import { IBook, IChapter, IVerse } from '../../common/types/types';
import EditBookModal from './editBook';
// import DeleteBookModal from './deleteBook';
import { useNavigate } from 'react-router-dom';
import { useUser } from '../../hooks/useUser';


export const BookComponentList = () => {

    const [books, setBooks] = useState<IBook[]>([]);
    // const [showNewBookModal, setShowNewBookModal] = useState(false);
    const [showEditBookModal, setShowEditBookModal] = useState(false);
    // const [showDeleteBookModal, setShowDeleteBookModal] = useState(false);
    const [informationReady, setInformationReady] = useState(false);
    const [showNotification, setShowNotification] = useState(false);
    const [notificationMessage, setNotificationMessage] = useState('');
    const [chaptersState, setChaptersState] = useState<Map<string, string>>(new Map());
    const [chaptersBeingImported, setChaptersBeingImported] = useState<string[]>([]);
    // const { selectedBook, setSelectedBook } = useContext(MyGlobalContext);
    const [selectedBook, setSelectedBook] = useState<IBook>({} as unknown as IBook);
    const navigate = useNavigate();
    const { isLogged } = useUser();
    // const chaptersState = new Map<string, string>();

    // const handleCloseNewBookModal = () => {
    //     retrieveBooks();
    //     setShowNewBookModal(false);
    // }

    const handleCloseEditBookModal = () => {
        retrieveBooks();
        setShowEditBookModal(false);
    }

    // const handleCloseDeleteBookModal = () => {
    //     retrieveBooks();
    //     setShowDeleteBookModal(false);
    // }

    useEffect(() => {
        (async () => {
            if (!isLogged) {
                navigate('/login');
            } else {
                setInformationReady(false);
                await retrieveBooks();

                setInformationReady(true);
            }
        })();
    }, []);



    const retrieveBooks = async () => {
        const data = await BookService.getAll();
        data.sort((a, b) => {
            if (parseInt(a.ordering) > parseInt(b.ordering)) {
                return 1;
            } else if (parseInt(a.ordering) < parseInt(b.ordering)) {
                return -1;
            }
            return 0;
        });
        data.forEach((book) => {
            book.chapters.forEach(chapter => {
                chaptersState.set(`${book.id}${chapter.number}`, 'Loaded');
            });
        })
        console.log(chaptersState);
        setBooks(data);
    }

    // const removeClick = (book: IBook) => {
    //     setShowDeleteBookModal(true);
    //     setSelectedBook(book)
    //     retrieveBooks();
    // }

    // const renderHeader = () => {
    //     return model.fields.map((key, index) => {
    //         if (key.code === 'longName')
    //             return <th key={index}>{key.name}</th>;
    //     });
    // };

    // const renderChapterHeader = () => {
    //     return chapterModel.fields.map((key, index) => {
    //         if (key.code != 'id')
    //             return <th key={index}>{key.name}</th>;
    //     });
    // };

    // const renderVerseHeader = () => {
    //     return verseModel.fields.map((key, index) => {
    //         if (key.code == 'content')
    //             return <th key={index}>{key.name}</th>;
    //     });
    // };


    // const updateClick = (book: IBook) => {
    //     setSelectedBook(book);
    //     setShowEditBookModal(true);
    // }

    // const addClick = () => {
    //     setShowNewBookModal(true);
    // }

    const importVerses = async (bookId: string, chapterNumber: string) => {
        const updatedMap = new Map(chaptersState);
        updatedMap.set(`${bookId}${chapterNumber}`, 'Loading');
        setChaptersState(updatedMap);
        try {
            const importedVerses = (await BookService.importVerses(bookId, chapterNumber)).data.body.verses;
            for (const book of books) {
                if (book.id === bookId) {
                    for (const chapter of book.chapters) {
                        if (chapter.number === chapterNumber) {
                            chapter.verses = importedVerses;
                        }
                    }
                }
            }


        } catch (error) {
            setShowNotification(true);
            console.log('error', error);
            setNotificationMessage(error.message);
        } finally {
            const updatedMap = new Map(chaptersState);
            updatedMap.set(`${bookId}${chapterNumber}`, 'Loaded');
            setChaptersState(updatedMap);
        }
    }

    const renderBody = () => {
        return (
            books &&
            books.map((book: IBook) => {
                return (
                    <tr key={book.id}>
                        <td>
                            <Accordion >
                                <Accordion.Item eventKey="0">
                                    <Accordion.Header>{`${book.longName} - (${book.classification})`}</Accordion.Header>
                                    <Accordion.Body>
                                        <Table striped bordered hover responsive id={`${chapterModel.name_plur}-table`}>
                                            <thead></thead>
                                            <tbody>{renderChapters(book.id, book.chapters)}</tbody>
                                            <tfoot></tfoot>
                                        </Table>
                                    </Accordion.Body>
                                </Accordion.Item>
                            </Accordion>
                        </td>
                        {/*<td>
                            <Button variant="primary" onClick={() => updateClick(book)}>
                                <span className="material-icons">Modificar</span>
                            </Button>
                             <Button variant="danger" onClick={() => removeClick(book)}>
                                <span className="material-icons">Eliminar</span>
                            </Button> 
                        </td>*/}
                    </tr>
                );
            })
        );
    }

    const setLoading = (chapterId: string) => {
        // console.log(chapterId);
        // console.log(chaptersBeingImported);
        return chaptersBeingImported.includes(chapterId);
    }

    const renderChapters = (bookId: string, chapters: IChapter[]) => {
        return (
            chapters &&
            chapters.map((chapter: IChapter) => {
                return (

                    <tr key={chapter.id}>
                        {chapter.number !== 'intro' &&
                            <td>
                                {chapter.verses && chapter.verses.length > 0 &&
                                    <Accordion >
                                        <Accordion.Item eventKey="0">
                                            <Accordion.Header>{chapter.reference}</Accordion.Header>
                                            <Accordion.Body>
                                                <Table striped bordered hover responsive id={`${chapterModel.name_plur}-table`}>
                                                    <thead></thead>
                                                    <tbody>{renderVerses(chapter.verses)}</tbody>
                                                    <tfoot></tfoot>
                                                </Table>
                                            </Accordion.Body>
                                        </Accordion.Item>
                                    </Accordion>
                                }
                                {(!chapter.verses || chapter.verses.length <= 0) &&
                                    <Button className="importButton" variant="primary" onClick={() => importVerses(bookId, chapter.number)} disabled={chaptersState.get(`${bookId}${chapter.number}`) === 'Loading'} >
                                        {chaptersState.get(`${bookId}${chapter.number}`) === 'Loading' &&
                                            <><Spinner
                                                as="span"
                                                animation="grow"
                                                size="sm"
                                                role="status"
                                                aria-hidden="true" /><span> Loading...</span>
                                            </>
                                        }
                                        {!(chaptersState.get(`${bookId}${chapter.number}`) === 'Loading') && `Importar ${chapter.reference}`}
                                    </Button>
                                }
                            </td>
                        }
                    </tr >
                );
            })
        );
    }

    const renderVerses = (verses: IVerse[]) => {
        return (
            verses &&
            verses.map((verse: IVerse) => {
                return (
                    <tr key={`${verse.id}`}>
                        <td className="verse-row">{verse.content}</td>
                    </tr>
                );
            })
        );
    }

    const closeNotification = () => {
        setShowNotification(false);
        setNotificationMessage('');
    }

    return (
        <>
            <ToastContainer
                className="position-fixed"
                position='top-center'
                style={{ zIndex: 1 }}
            ><Toast bg='danger' onClose={closeNotification} show={showNotification} delay={3000} autohide>
                    <Toast.Header>
                        <img src="holder.js/20x20?text=%20" className="rounded me-2" alt="" />
                        <strong className="me-auto">Error al procesar la transacción</strong>
                    </Toast.Header>
                    <Toast.Body>{notificationMessage}</Toast.Body>
                </Toast >
            </ToastContainer>
            <Card body>
                {/* <Button className="addButton" variant="primary" onClick={() => addClick()}>
                    Nuevo Libro
                </Button> */}
                <p className="headerLbl">{`${diccionary.list_lbl} ${model.name_plur}`}</p>
            </Card>
            {informationReady && <Table striped bordered hover responsive id={`${model.name_plur}-table`}>

                <thead></thead>
                <tbody>{renderBody()}</tbody>
                <tfoot></tfoot>
            </Table>
            }
            {!informationReady &&
                <Container fluid className='spinner-books'>
                    <Spinner animation="grow" />
                </Container>}
            {/* {showNewBookModal && <NewBookModal show={showNewBookModal} handleClose={handleCloseNewBookModal}></NewBookModal>} */}
            {showEditBookModal && selectedBook && <EditBookModal show={showEditBookModal} handleClose={handleCloseEditBookModal} book={selectedBook}></EditBookModal>}
            {/* {showDeleteBookModal && selectedBook.id && <DeleteBookModal show={showDeleteBookModal} handleClose={handleCloseDeleteBookModal} bookId={selectedBook.id}></DeleteBookModal>} */}
        </>
    );

}

export default BookComponentList;