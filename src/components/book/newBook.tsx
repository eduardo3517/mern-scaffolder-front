import React from 'react'
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { diccionary } from "../../config";
import model from "../../models/book";
import { useState } from "react";
import BookService from "../../services/bookService";
import { IField, IBook, INewModalProps } from '../../common/types/types';

const NewBookModal = (props: INewModalProps) => {
    const emptyBook = {
        id: '',
        abbreviation: '',
        chapters: [],
        code: '',
        longName: '',
        shortName: '',
        classification: '',
        ordering: '',
    }
    const [book, setBook] = useState(emptyBook);


    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setBook({ ...book, [name]: value });
    };

    const saveData = async () => {
        if (book) {
            await BookService.create(book as IBook);
        }
        props.handleClose();
    }


    const setValue = (namep: string) => {
        switch (namep) {
            case "abbreviation":
                return book.abbreviation;
            case "code":
                return book.code;
            case "longName":
                return book.longName;
            case "shortName":
                return book.shortName;
            case "classification":
                return book.classification;
            case "ordering":
                return book.ordering;
            default:
                return "";
        }
    }

    const renderModalBody = () => {
        return (
            <>
                {`${diccionary.create_book_instructions}`}
                {
                    model.fields.map((field: IField) => {
                        if (field.code !== "id") {
                            return <>
                                <Form.Group className="mb-3" controlId={`formBasic${field.code}`} key={`formBasic${field.code}`}>
                                    <Form.Label>{field.name}</Form.Label>
                                    <Form.Control value={setValue(field.code)} type={field.type} placeholder={field.name} onChange={handleInputChange} name={field.code} />
                                </Form.Group>
                            </>;
                        }
                        else {
                            return <></>
                        }

                    })
                }
            </>
        );
    }

    return (
        <>
            <Modal show={props.show} onHide={props.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{`${diccionary.create_book_title}`}</Modal.Title>
                </Modal.Header>

                <Form >
                    <Modal.Body>
                        {renderModalBody()}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={props.handleClose}>
                            {diccionary.cancel_btn_lbl}
                        </Button>
                        <Button variant="primary" onClick={saveData}>
                            {diccionary.save_btn_lbl}
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}

export default NewBookModal;