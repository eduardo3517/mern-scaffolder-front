import React from 'react'
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { classifications, diccionary, fieldTypes } from "../../config";
import model from "../../models/book";
import { useState } from "react";
import BookService from "../../services/bookService";
import { IEditBookProps, IField, IBook, IClassification } from '../../common/types/types';
// import { MyGlobalContext } from '../../context/staticContext';

const EditBookModal = (props: IEditBookProps) => {

    const [book, setBook] = useState<IBook>(props.book);
    // const { setSelectedBook } = useContext(MyGlobalContext);

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setBook({ ...book, [name]: value });
    };

    const handleSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        book.classification = event.target.value;
    }

    const saveData = async () => {
        if (book && book.id) {
            await BookService.update(book.id, book);
            // setSelectedBook(book);
        }
        props.handleClose();
    }

    const setValue = (namep: string) => {
        switch (namep) {
            case "abbreviation":
                return book.abbreviation;
            case "code":
                return book.code;
            case "longName":
                return book.longName;
            case "shortName":
                return book.shortName;
            case "classification":
                return book.classification;
            case "ordering":
                return book.ordering;
            default:
                return "";
        }
    }

    const renderModalBody = () => {
        return (
            <>
                {`${diccionary.edit_book_instructions}`}
                {
                    model.fields.map((field: IField) => {
                        if (field.code != 'id') {
                            if (field.type === fieldTypes.textInput) {
                                return <>
                                    <Form.Group className="mb-3" controlId={`formBasic${field.code}`} key={`formBasic${field.code}`}>
                                        <Form.Label>{field.name}</Form.Label>
                                        <Form.Control value={setValue(field.code)} type={field.type} placeholder={field.name} onChange={handleInputChange} name={field.code} />
                                    </Form.Group>
                                </>;
                            } else if (field.type === fieldTypes.dropdown) {
                                if (field.code != 'chapters')
                                    return <>
                                        <Form.Label>{field.name}</Form.Label>
                                        <Form.Select aria-label="Seleccione la clasificación" onChange={handleSelectChange}>
                                            <option>Open this select menu</option>
                                            {classifications.map((classi: IClassification) => {
                                                return <option value={classi.code} key={classi.code} defaultValue={book.classification}>{classi.description}</option>
                                            }
                                            )}
                                        </Form.Select>
                                    </>;
                            }
                        }

                    })
                }
            </>
        );
    }

    return (
        <Modal show={props.show} onHide={props.handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>{`${diccionary.edit_book_title}`}</Modal.Title>
            </Modal.Header>
            <Form >
                <Modal.Body>
                    {renderModalBody()}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={props.handleClose}>
                        {diccionary.cancel_btn_lbl}
                    </Button>
                    <Button variant="primary" onClick={saveData}>
                        {diccionary.save_btn_lbl}
                    </Button>
                </Modal.Footer>
            </Form>
        </Modal>
    )
}

export default EditBookModal;