import React from 'react'
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { diccionary } from "../../config";
import BookService from "../../services/bookService";
import { IDeleteBookProps } from '../../common/types/types';

const DeleteBookModal = (props: IDeleteBookProps) => {

    const removeBook = async () => {
        await BookService.deleteRecord(props.bookId);
        props.handleClose();
    }

    return (
        <>
            <Modal show={props.show} onHide={props.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{`${diccionary.delete_book_title}`}</Modal.Title>
                </Modal.Header>

                <Form >
                    <Modal.Body>
                        {`${diccionary.delete_book_confirmation_msg}`}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={props.handleClose}>
                            {diccionary.cancel_btn_lbl}
                        </Button>
                        <Button variant="danger" onClick={removeBook}>
                            {diccionary.delete_btn_lbl}
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}

export default DeleteBookModal;