import React, { useContext } from 'react'
import { useState, useEffect } from "react";
import ProjectService from "../../services/projectService";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { diccionary } from "../../config";
import NewProjectModal from "./newProject";
import model from "../../models/project";
import { IProject } from '../../common/types/types';
import EditProjectModal from './editProject';
import DeleteProjectModal from './deleteProject';
import { MyGlobalContext } from '../../context/staticContext';
import { Link, useNavigate } from 'react-router-dom';
import { useUser } from '../../hooks/useUser';


export const ProjectComponentList = () => {

    const [projects, setProjects] = useState<IProject[]>([]);
    const [showNewProjectModal, setShowNewProjectModal] = useState(false);
    const [showEditProjectModal, setShowEditProjectModal] = useState(false);
    const [showDeleteProjectModal, setShowDeleteProjectModal] = useState(false);
    const { selectedProject, setSelectedProject } = useContext(MyGlobalContext);
    const navigate = useNavigate();
    const { isLogged } = useUser();

    const handleCloseNewProjectModal = () => {
        retrieveProjects();
        setShowNewProjectModal(false);
    }

    const handleCloseEditProjectModal = () => {
        retrieveProjects();
        setShowEditProjectModal(false);
    }

    const handleCloseDeleteProjectModal = () => {
        retrieveProjects();
        setShowDeleteProjectModal(false);
    }

    useEffect(() => {
        if (!isLogged) navigate('/login')
        else retrieveProjects();
    }, []);



    const retrieveProjects = async () => {
        const data = await ProjectService.getAll();
        setProjects(data);
    }

    const removeClick = (project: IProject) => {
        setShowDeleteProjectModal(true);
        setSelectedProject(project)
        retrieveProjects();
    }

    const renderHeader = () => {
        return model.fields.map((key, index) => {
            return <th key={index}>{key.name}</th>;
        });
    };

    const updateClick = (project: IProject) => {
        setSelectedProject(project);
        setShowEditProjectModal(true);
    }

    const addClick = () => {
        setShowNewProjectModal(true);
    }

    const renderBody = () => {
        return (
            projects &&
            projects.map((project: IProject) => {
                return (
                    <tr key={project.id}>
                        <td>{project.id}</td>
                        <td>{project.projectName}</td>
                        <td>{project.server}</td>
                        <td>{project.db_name}</td>
                        <td>{project.db_user}</td>
                        <td>{project.pass}</td>
                        <td>
                            <Button variant="danger" onClick={() => removeClick(project)}>
                                <span className="material-icons">Eliminar</span>
                            </Button>
                            <Button variant="primary" onClick={() => updateClick(project)}>
                                <span className="material-icons">Modificar</span>
                            </Button>
                            <Link to="/entities">
                                <Button variant="secondary" onClick={() => setSelectedProject(project)}>
                                    <span className="material-icons">Entidades Asociadas</span>
                                </Button>
                            </Link>
                        </td>
                    </tr>
                );
            })
        );

    }

    return (
        <>
            <Card body>
                <Button className="addButton" variant="primary" onClick={() => addClick()}>
                    Nuevo Proyecto
                </Button>
                <p className="headerLbl">{`${diccionary.list_lbl} ${model.name_plur}`}</p>
            </Card>
            <Table striped bordered hover id={`${model.name_plur}-table`}>
                <thead>
                    <tr>
                        {renderHeader()}
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>{renderBody()}</tbody>
            </Table>

            {showNewProjectModal && <NewProjectModal show={showNewProjectModal} handleClose={handleCloseNewProjectModal}></NewProjectModal>}
            {showEditProjectModal && selectedProject && <EditProjectModal show={showEditProjectModal} handleClose={handleCloseEditProjectModal} project={selectedProject}></EditProjectModal>}
            {showDeleteProjectModal && selectedProject.id && <DeleteProjectModal show={showDeleteProjectModal} handleClose={handleCloseDeleteProjectModal} projectId={selectedProject.id}></DeleteProjectModal>}
        </>
    );

}

export default ProjectComponentList;