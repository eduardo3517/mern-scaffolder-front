import React from 'react'
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { diccionary } from "../../config";
import ProjectService from "../../services/projectService";
import { IDeleteProjectProps } from '../../common/types/types';

const DeleteProjectModal = (props: IDeleteProjectProps) => {

    const removeProject = async () => {
        await ProjectService.deleteRecord(props.projectId);
        props.handleClose();
    }

    return (
        <>
            <Modal show={props.show} onHide={props.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{`${diccionary.delete_project_title}`}</Modal.Title>
                </Modal.Header>

                <Form >
                    <Modal.Body>
                        {`${diccionary.delete_project_confirmation_msg}`}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={props.handleClose}>
                            {diccionary.cancel_btn_lbl}
                        </Button>
                        <Button variant="danger" onClick={removeProject}>
                            {diccionary.delete_btn_lbl}
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}

export default DeleteProjectModal;