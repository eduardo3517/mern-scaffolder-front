import React, { useContext } from 'react'
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { diccionary } from "../../config";
import model from "../../models/project";
import { useState } from "react";
import ProjectService from "../../services/projectService";
import { IEditProjectProps, IField, IProject } from '../../common/types/types';
import { MyGlobalContext } from '../../context/staticContext';

const EditProjectModal = (props: IEditProjectProps) => {

    const [project, setProject] = useState<IProject>(props.project);
    const { setSelectedProject } = useContext(MyGlobalContext);

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setProject({ ...project, [name]: value });
    };

    const saveData = async () => {
        if (project && project.id) {
            await ProjectService.update(project.id, project);
            setSelectedProject(project);
        }
        props.handleClose();

    }


    const setValue = (namep: string) => {
        switch (namep) {
            case "projectName":
                return project.projectName;
            case "server":
                return project.server;
            case "db_name":
                return project.db_name;
            case "db_user":
                return project.db_user;
            case "pass":
                return project.pass;
            default:
                return "";
        }
    }

    const renderModalBody = () => {
        return (
            <>
                {`${diccionary.edit_project_instructions}`}
                {
                    model.fields.map((field: IField) => {
                        if (field.code !== "id") {
                            return <>
                                <Form.Group className="mb-3" controlId={`formBasic${field.code}`} key={`formBasic${field.code}`}>
                                    <Form.Label>{field.name}</Form.Label>
                                    <Form.Control value={setValue(field.code)} type={field.type} placeholder={field.name} onChange={handleInputChange} name={field.code} />
                                </Form.Group>
                            </>;
                        }
                        else {
                            return <></>
                        }

                    })
                }
            </>
        );
    }

    return (
        <>
            <Modal show={props.show} onHide={props.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{`${diccionary.edit_project_title}`}</Modal.Title>
                </Modal.Header>
                <Form >
                    <Modal.Body>
                        {renderModalBody()}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={props.handleClose}>
                            {diccionary.cancel_btn_lbl}
                        </Button>
                        <Button variant="primary" onClick={saveData}>
                            {diccionary.save_btn_lbl}
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}

export default EditProjectModal;