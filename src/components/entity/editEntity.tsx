import React, { useEffect } from 'react'
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { diccionary, fieldTypes } from "../../config";
import model from "../../models/entity";
import { useState } from "react";
import { IEditEntityProps, IEntity, IField, IProject } from '../../common/types/types';
import EntityService from '../../services/entityService';
import ProjectService from '../../services/projectService';

const EditEntityModal = (props: IEditEntityProps) => {

    const [entity, setEntity] = useState<IEntity>(props.entity);
    const [projects, setProjects] = useState<IProject[]>([]);

    useEffect(() => {
        retrieveProjects();
    }, []);

    const retrieveProjects = async () => {
        const data = await ProjectService.getAll();
        setProjects(data);
    }

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setEntity({ ...entity, [name]: value });
    };

    const handleSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        entity.projectId = event.target.value;
    }

    const saveData = async () => {
        if (entity && entity.id) {
            await EntityService.update(entity.id, entity);
        }
        props.handleClose();
    }


    const setValue = (namep: string) => {
        switch (namep) {
            case "entityName":
                return entity.entityName;
            case "projectId":
                return entity.projectId;
            default:
                return "";
        }
    }

    const renderModalBody = () => {
        return (
            <>
                {`${diccionary.edit_entity_instructions}`}
                {
                    model.fields.map((field: IField) => {
                        if (field.code !== "id") {
                            if (field.type === fieldTypes.textInput) {
                                return <>
                                    <Form.Group className="mb-3" controlId={`formBasic${field.code}`} key={`formBasic${field.code}`}>
                                        <Form.Label>{field.name}</Form.Label>
                                        <Form.Control value={setValue(field.code)} type={field.type} placeholder={field.name} onChange={handleInputChange} name={field.code} />
                                    </Form.Group>
                                </>;
                            } else if (field.type === fieldTypes.dropdown) {
                                return <>
                                    <Form.Label>{field.name}</Form.Label>
                                    <Form.Select aria-label="Seleccione el proyecto" onChange={handleSelectChange}>
                                        <option>Open this select menu</option>
                                        {projects.map((project: IProject) => {
                                            return <option value={project.id} key={project.id} selected={project.id === entity.projectId}>{project.projectName}</option>
                                        }
                                        )}
                                    </Form.Select>
                                </>;
                            }
                        }
                        else {
                            return <></>
                        }

                    })
                }
            </>
        );
    }

    return (
        <>
            <Modal show={props.show} onHide={props.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{`${diccionary.edit_entity_title}`}</Modal.Title>
                </Modal.Header>
                <Form >
                    <Modal.Body>
                        {renderModalBody()}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={props.handleClose}>
                            {diccionary.cancel_btn_lbl}
                        </Button>
                        <Button variant="primary" onClick={saveData}>
                            {diccionary.save_btn_lbl}
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}

export default EditEntityModal;