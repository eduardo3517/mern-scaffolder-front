import React, { useContext } from 'react'
import { useState, useEffect } from "react";
import EntityService from "../../services/entityService";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { diccionary } from "../../config";
import NewEntityModal from "./newEntity";
import model from "../../models/entity";
import { IEntity, IProject } from '../../common/types/types';
import DeleteEntityModal from './deleteEntity';
import EditEntityModal from './editEntity';
import { MyGlobalContext } from '../../context/staticContext';
import EditProjectModal from '../project/editProject';
import ProjectService from '../../services/projectService';
import { useUser } from '../../hooks/useUser';
import { useNavigate } from 'react-router';


export const ProjectComponentList = () => {
    const [entities, setEntities] = useState<IEntity[]>([]);
    const [showNewEntityModal, setShowNewEntityModal] = useState(false);
    const [showEditEntityModal, setShowEditEntityModal] = useState(false);
    const [showDeleteEntityModal, setShowDeleteEntityModal] = useState(false);
    const [showProjectModal, setShowProjectModal] = useState(false);
    const [selectedEntity, setSelectedEntity] = useState<IEntity>({} as unknown as IEntity);
    const [selectedReadProject, setSelectedReadProject] = useState<IProject>({} as unknown as IProject)
    const { selectedProject } = useContext(MyGlobalContext);
    const navigate = useNavigate();
    const { isLogged } = useUser();

    const handleCloseNewEntityModal = () => {
        retrieveEntities();
        setShowNewEntityModal(false);
    }

    const handleCloseEditEntityModal = () => {
        retrieveEntities();
        setShowEditEntityModal(false);
    }

    const handleCloseDeleteEntityModal = () => {
        retrieveEntities();
        setShowDeleteEntityModal(false);
    }

    useEffect(() => {
        if (!isLogged) navigate('/login')
        else retrieveEntities();
    }, []);

    const retrieveEntities = async () => {
        const data = await EntityService.getAll();
        if (selectedProject.id) {
            data.filter((entity) => entity.projectId === selectedProject.id)
        }
        setEntities(data);
    }

    const removeClick = (entity: IEntity) => {
        setShowDeleteEntityModal(true);
        setSelectedEntity(entity)
        retrieveEntities();
    }

    const renderHeader = () => {
        return model.fields.map((key, index) => {
            return <th key={index}>{key.name}</th>;
        });
    };

    const updateClick = (entity: IEntity) => {
        setSelectedEntity(entity);
        setShowEditEntityModal(true);
    }

    const addClick = () => {
        setShowNewEntityModal(true);
    }

    const openProjectModal = async (projectId: string) => {
        const projectd = await ProjectService.get(projectId);
        setSelectedReadProject(projectd);
        setShowProjectModal(true);
    }
    const handleCloseProjectModal = () => {
        setShowProjectModal(false);
    }

    const renderBody = () => {
        return (
            entities &&
            entities.map((entity: IEntity) => {
                return (
                    <tr key={entity.id}>
                        <td>{entity.id}</td>
                        <td>{entity.entityName}</td>
                        <td><Button variant='link' onClick={() => openProjectModal(entity.projectId)}>{entity.projectId}</Button></td>
                        <td>
                            <Button variant="danger" onClick={() => removeClick(entity)}>
                                <span className="material-icons">Borrar</span>
                            </Button>
                            <Button variant="primary" onClick={() => updateClick(entity)}>
                                <span className="material-icons">Modificar</span>
                            </Button>
                        </td>
                    </tr>
                );
            })
        );

    }

    return (
        <>
            <Card body>
                <Button className="addButton" variant="primary" onClick={() => addClick()}>
                    Nueva Entidad
                </Button>
                <p className="headerLbl">{`${diccionary.list_lbl} ${model.name_plur}`}</p>
            </Card>
            <Table striped bordered hover id={`${model.name_plur}-table`}>
                <thead>
                    <tr>
                        {renderHeader()}
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>{renderBody()}</tbody>
            </Table>

            {showNewEntityModal && <NewEntityModal show={showNewEntityModal} handleClose={handleCloseNewEntityModal}></NewEntityModal>}
            {showEditEntityModal && selectedEntity && <EditEntityModal show={showEditEntityModal} handleClose={handleCloseEditEntityModal} entity={selectedEntity}></EditEntityModal>}
            {showDeleteEntityModal && selectedEntity.id && <DeleteEntityModal show={showDeleteEntityModal} handleClose={handleCloseDeleteEntityModal} entityId={selectedEntity.id}></DeleteEntityModal>}
            {showProjectModal && <EditProjectModal show={setShowProjectModal} handleClose={handleCloseProjectModal} project={selectedReadProject}></EditProjectModal>}
        </>
    );

}

export default ProjectComponentList;