import React from 'react'
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { diccionary } from "../../config";
import EntityService from "../../services/entityService";
import { IDeleteEntityProps } from '../../common/types/types';

const DeleteEntityModal = (props: IDeleteEntityProps) => {

    const removeEntity = async () => {
        await EntityService.deleteRecord(props.entityId);
        props.handleClose();
    }

    return (
        <>
            <Modal show={props.show} onHide={props.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{`${diccionary.delete_entity_title}`}</Modal.Title>
                </Modal.Header>

                <Form >
                    <Modal.Body>
                        {`${diccionary.delete_entity_confirmation_msg}`}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={props.handleClose}>
                            {diccionary.cancel_btn_lbl}
                        </Button>
                        <Button variant="danger" onClick={removeEntity}>
                            {diccionary.delete_btn_lbl}
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}

export default DeleteEntityModal;