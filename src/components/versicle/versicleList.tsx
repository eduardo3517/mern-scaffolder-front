import React from 'react'
import { useState, useEffect } from "react";
import VersicleService from "../../services/versicleService";
import { Table, Button, Card } from "react-bootstrap";
import { diccionary } from "../../config";
import NewVersicleModal from "./newVersicle";
import model from "../../models/versicle";
import { IVersicle } from '../../common/types/types';
import { useNavigate } from 'react-router-dom';
import { useUser } from '../../hooks/useUser';
import DeleteVersicleModal from './deleteVersicle';
import EditVersicleModal from './editVersicle';


export const VersicleComponentList = () => {

    const [versicles, setVersicles] = useState<IVersicle[]>([]);
    const [showNewVersicleModal, setShowNewVersicleModal] = useState(false);
    const [showEditVersicleModal, setShowEditVersicleModal] = useState(false);
    const [showDeleteVersicleModal, setShowDeleteVersicleModal] = useState(false);
    const [selectedVersicle, setSelectedVersicle] = useState<IVersicle>({} as unknown as IVersicle);
    // const { selectedProject, setSelectedProject } = useContext(MyGlobalContext);
    const navigate = useNavigate();
    const { isLogged } = useUser();

    const handleCloseNewVersicleModal = () => {
        retrieveVersicles();
        setShowNewVersicleModal(false);
    }

    const handleCloseEditVersicleModal = () => {
        retrieveVersicles();
        setShowEditVersicleModal(false);
    }

    const handleCloseDeleteVersicleModal = () => {
        retrieveVersicles();
        setShowDeleteVersicleModal(false);
    }

    useEffect(() => {
        if (!isLogged) navigate('/login')
        else retrieveVersicles();
    }, []);

    const retrieveVersicles = async () => {
        const data = await VersicleService.getAll();
        setVersicles(data);
    }

    const removeClick = (vers: IVersicle) => {
        setShowDeleteVersicleModal(true);
        setSelectedVersicle(vers)
        retrieveVersicles();
    }

    const renderHeader = () => {
        return model.fields.map((key, index) => {
            return <th key={index}>{key.name}</th>;
        });
    };

    const updateClick = (vers: IVersicle) => {
        setSelectedVersicle(vers);
        setShowEditVersicleModal(true);
    }

    const addClick = () => {
        setShowNewVersicleModal(true);
    }

    const renderBody = () => {
        return (
            versicles &&
            versicles.map((vers: IVersicle) => {
                return (
                    <tr key={vers.id}>
                        <td>{vers.id}</td>
                        <td>{vers.book}</td>
                        <td>{vers.number}</td>
                        <td>{vers.rvr1960}</td>
                        <td>{vers.ntv}</td>
                        <td>{vers.lbla}</td>
                        <td>
                            <Button variant="danger" onClick={() => removeClick(vers)}>
                                <span className="material-icons">Eliminar</span>
                            </Button>
                            <Button variant="primary" onClick={() => updateClick(vers)}>
                                <span className="material-icons">Modificar</span>
                            </Button>
                        </td>
                    </tr>
                );
            })
        );

    }

    return (
        <>
            <Card body>
                <Button className="addButton" variant="primary" onClick={() => addClick()}>
                    Nuevo Versículo
                </Button>
                <p className="headerLbl">{`${diccionary.list_lbl} ${model.name_plur}`}</p>
            </Card>
            <Table striped bordered hover id={`${model.name_plur}-table`}>
                <thead>
                    <tr>
                        {renderHeader()}
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>{renderBody()}</tbody>
            </Table>

            {showNewVersicleModal && <NewVersicleModal show={showNewVersicleModal} handleClose={handleCloseNewVersicleModal}></NewVersicleModal>}
            {showEditVersicleModal && selectedVersicle && <EditVersicleModal show={showEditVersicleModal} handleClose={handleCloseEditVersicleModal} versicle={selectedVersicle}></EditVersicleModal>}
            {showDeleteVersicleModal && selectedVersicle.id && <DeleteVersicleModal show={showDeleteVersicleModal} handleClose={handleCloseDeleteVersicleModal} versicleId={selectedVersicle.id}></DeleteVersicleModal>}
        </>
    );

}

export default VersicleComponentList;