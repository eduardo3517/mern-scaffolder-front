import React from 'react'
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { diccionary } from "../../config";
import model from "../../models/versicle";
import { useState } from "react";
import VersicleService from "../../services/versicleService";
import { IEditVersicleProps, IField, IVersicle } from '../../common/types/types';
// import { MyGlobalContext } from '../../context/staticContext';

const EditVersicleModal = (props: IEditVersicleProps) => {

    const [versicle, setVersicle] = useState<IVersicle>(props.versicle);
    // const { setSelectedProject } = useContext(MyGlobalContext);

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setVersicle({ ...versicle, [name]: value });
    };

    const saveData = async () => {
        if (versicle && versicle.id) {
            await VersicleService.update(versicle.id, versicle);
            // setSelectedProject(versicle);
        }
        props.handleClose();

    }


    const setValue = (namep: string) => {
        switch (namep) {
            case "projectName":
                return versicle.book;
            case "server":
                return versicle.number;
            case "db_name":
                return versicle.rvr1960;
            case "db_user":
                return versicle.ntv;
            case "pass":
                return versicle.lbla;
            default:
                return "";
        }
    }

    const renderModalBody = () => {
        return (
            <>
                {`${diccionary.edit_versicle_instructions}`}
                {
                    model.fields.map((field: IField) => {
                        if (field.code !== "id") {
                            return <>
                                <Form.Group className="mb-3" controlId={`formBasic${field.code}`} key={`formBasic${field.code}`}>
                                    <Form.Label>{field.name}</Form.Label>
                                    <Form.Control value={setValue(field.code)} type={field.type} placeholder={field.name} onChange={handleInputChange} name={field.code} />
                                </Form.Group>
                            </>;
                        }
                        else {
                            return <></>
                        }

                    })
                }
            </>
        );
    }

    return (
        <>
            <Modal show={props.show} onHide={props.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{`${diccionary.edit_project_title}`}</Modal.Title>
                </Modal.Header>
                <Form >
                    <Modal.Body>
                        {renderModalBody()}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={props.handleClose}>
                            {diccionary.cancel_btn_lbl}
                        </Button>
                        <Button variant="primary" onClick={saveData}>
                            {diccionary.save_btn_lbl}
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}

export default EditVersicleModal;