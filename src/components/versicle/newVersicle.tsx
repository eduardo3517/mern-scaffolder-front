import React from 'react'
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { diccionary, fieldTypes } from "../../config";
import model from "../../models/versicle";
import { useState } from "react";
import VersicleService from "../../services/versicleService";
import { IField, INewModalProps, IVersicle } from '../../common/types/types';
import { books } from '../../common/dictionaries/books';

const NewVersicleModal = (props: INewModalProps) => {
    const emptyVersicle = {
        id: '',
        book: '',
        number: '',
        rvr1960: '',
        ntv: '',
        lbla: ''
    }
    const [versicle, setVersicle] = useState(emptyVersicle);


    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        setVersicle({ ...versicle, [name]: value });
    };

    const saveData = async () => {
        if (versicle) {
            await VersicleService.create(versicle as IVersicle);
        }
        props.handleClose();
    }

    const handleSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        versicle.book = event.target.value;
    }


    const setValue = (namep: string) => {
        switch (namep) {
            case 'book':
                return versicle.book;
            case "number":
                return versicle.number;
            case "rvr1960":
                return versicle.rvr1960;
            case "ntv":
                return versicle.ntv;
            case "lbla":
                return versicle.lbla;
            default:
                return "";
        }
    }

    const renderModalBody = () => {
        return (
            <>
                {`${diccionary.create_versicle_instructions}`}
                {
                    model.fields.map((field: IField) => {

                        console.log(field)
                        if (field.code !== "id") {
                            if (field.type === fieldTypes.textInput) {
                                return <>
                                    <Form.Group className="mb-3" controlId={`formBasic${field.code}`} key={`formBasic${field.code}`}>
                                        <Form.Label>{field.name}</Form.Label>
                                        <Form.Control value={setValue(field.code)} type={field.type} placeholder={field.name} onChange={handleInputChange} name={field.code} />
                                    </Form.Group>
                                </>;
                            } else if (field.type === fieldTypes.number) {
                                return <>
                                    <Form.Group className="mb-3" controlId={`formBasic${field.code}`} key={`formBasic${field.code}`}>
                                        <Form.Label>{field.name}</Form.Label>
                                        <Form.Control type='number' onChange={handleInputChange} name={field.code} />
                                    </Form.Group>
                                </>;
                            } else if (field.type === fieldTypes.textArea) {
                                return <>
                                    <Form.Group className="mb-3" controlId={`formBasic${field.code}`} key={`formBasic${field.code}`}>
                                        <Form.Label>{field.name}</Form.Label>
                                        <Form.Control as="textarea" rows={3} onChange={handleInputChange} name={field.code} />
                                    </Form.Group>
                                </>;
                            } else if (field.type === fieldTypes.dropdown) {
                                return <>
                                    <Form.Select aria-label="Seleccione el libro" onChange={handleSelectChange}>
                                        <option>Open this select menu</option>
                                        {books.map((book) => {
                                            return <option value={book.code} key={book.code}>{book.completeName}</option>
                                        }
                                        )}
                                    </Form.Select>
                                </>;
                            }
                        }
                    })

                }
            </>
        );
    }

    return (
        <>
            <Modal show={props.show} onHide={props.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{`${diccionary.create_versicle_title}`}</Modal.Title>
                </Modal.Header>

                <Form >
                    <Modal.Body>
                        {renderModalBody()}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={props.handleClose}>
                            {diccionary.cancel_btn_lbl}
                        </Button>
                        <Button variant="primary" onClick={saveData}>
                            {diccionary.save_btn_lbl}
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}

export default NewVersicleModal;