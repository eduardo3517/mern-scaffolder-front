import React from 'react'
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { diccionary } from "../../config";
import VersicleService from "../../services/versicleService";
import { IDeleteVersicleProps } from '../../common/types/types';

const DeleteVersicleModal = (props: IDeleteVersicleProps) => {

    const removeVersicle = async () => {
        await VersicleService.deleteRecord(props.versicleId);
        props.handleClose();
    }

    return (
        <>
            <Modal show={props.show} onHide={props.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{`${diccionary.delete_versicle_title}`}</Modal.Title>
                </Modal.Header>

                <Form >
                    <Modal.Body>
                        {`${diccionary.delete_versicle_confirmation_msg}`}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={props.handleClose}>
                            {diccionary.cancel_btn_lbl}
                        </Button>
                        <Button variant="danger" onClick={removeVersicle}>
                            {diccionary.delete_btn_lbl}
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    )
}

export default DeleteVersicleModal;