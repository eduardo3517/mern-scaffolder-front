import { IClassification } from "./common/types/types";

export const diccionary = {
    list_lbl: 'Listado de ',
    create_entity_title: 'Crear Entidad',
    create_entity_instructions: 'Por favor diligencie el siguiente formulario para la creación del registro.',
    save_btn_lbl: 'Guardar',
    cancel_btn_lbl: 'Cancelar',
    delete_btn_lbl: 'Eliminar',
    edit_project_title: 'Modificar Proyecto',
    create_project_title: 'Crear nuevo proyecto',
    create_project_instructions: 'Por favor diligencie el siguiente formulario para la creación del proyecto.',
    edit_project_instructions: 'Por favor modifique los campos de este formulario para actualizar el proyecto.',
    delete_project_title: 'Eliminar Proyecto',
    delete_project_confirmation_msg: '¿Está seguro que desea eliminar este proyecto?',
    delete_entity_title: 'Eliminar Entidad',
    delete_entity_confirmation_msg: '¿Está seguro que desea eliminar este proyecto?',
    edit_entity_instructions: 'Por favor modifique los campos de este formulario para actualizar la entidad.',
    edit_entity_title: 'Modificar Entidad',
    delete_versicle_title: 'Eliminar Versículo',
    delete_versicle_confirmation_msg: '¿Está seguro que desea eliminar este versículo?',
    create_versicle_instructions: 'Por favor diligencie el siguiente formulario para la creación del versículo.',
    create_versicle_title: 'Crear nuevo versiculo',
    edit_versicle_instructions: 'Por favor modifique los campos de este formulario para actualizar el versículo.',
    edit_versicle_title: 'Modificar Versículo',
    create_book_instructions: 'Por favor diligencie el siguiente formulario para la creación del libro',
    create_book_title: 'Crear Nuevo Libro',
    delete_book_title: 'Eliminar Libro',
    delete_book_confirmation_msg: '¿Está seguro que desea eliminar este libro?',
    edit_book_instructions: 'Por favor modifique los campos de este formulario para actualizar el libro.',
    edit_book_title: 'Modificar Libro',
}

export const enum fieldTypes {
    textInput = "text",
    dropdown = "dropdown",
    number = "number",
    textArea = "textarea"
}

export const classifications: IClassification[] = [
    {
        code: 'pt',
        description: 'Pentatéuco'
    },
    {
        code: 'ht',
        description: 'Históricos'
    },
    {
        code: 'sp',
        description: 'Sapiensales'
    },
    {
        code: 'pf',
        description: 'Proféticos'
    },
    {
        code: 'ev',
        description: 'Evangelios'
    },
    {
        code: 'hc',
        description: 'Hechos de los apóstoles'
    },
    {
        code: 'ep',
        description: 'Epístolas'
    },
    {
        code: 'ap',
        description: 'Apocalípsis'
    }
]
