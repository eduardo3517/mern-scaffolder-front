import { useCallback, useContext, useState } from "react";
import Context, { UserContextType } from "../context/userContext";
import { jwtDecode } from 'jwt-decode';
import UserService from '../services/userService';


export const useUser = () => {
    const { jwt, setJwt } = useContext(Context) as UserContextType;
    const [hasLoginError, setHasLoginError] = useState(false);
    const login = useCallback(async (email: string, password: string) => {
        const rpta = await UserService.login(email, password);
        if (rpta.status != 200) {
            setHasLoginError(true);
        } else {
            setHasLoginError(false);
            localStorage.setItem('token', rpta.body.token);
            setJwt(rpta.body.token);
            console.log('token', localStorage.getItem('token'));
        }
    }, [setJwt]);

    const logout = useCallback(async () => {
        setJwt(null);
        localStorage.removeItem('token');

    }, [setJwt])

    const isLoggedF = () => {
        const token = localStorage.getItem('token');
        const isLoggedToken = token != null && token != '' && jwtDecode(token || '').exp || 0 * 1000 > Date.now();
        
        const isLoggedJWT = jwt != null && jwt != '' && jwtDecode(jwt || '').exp || 0 * 1000 > Date.now();
        return isLoggedToken || isLoggedJWT;
    }
    return {
        isLogged: isLoggedF(),
        login,
        logout,
        hasLoginError,
    }
}