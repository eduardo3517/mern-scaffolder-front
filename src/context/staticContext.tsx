import { createContext, useContext } from "react"
import { IProject } from "../common/types/types"
export type GlobalContent = {
    selectedProject: IProject
    setSelectedProject: (c: IProject) => void
}
export const MyGlobalContext = createContext<GlobalContent>({

    selectedProject: {} as IProject, // set a default value
    setSelectedProject: () => {
        return;
    },
})
export const useGlobalContext = () => useContext(MyGlobalContext)