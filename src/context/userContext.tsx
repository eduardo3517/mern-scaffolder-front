import React, { ReactNode, useState } from "react";

export type UserContextType = {
    jwt: string | null;
    setJwt: React.Dispatch<React.SetStateAction<string | null>>;
}

const Context = React.createContext<UserContextType | undefined>(undefined)

type UserContextProviderProps = {
    children: ReactNode;
};
export const UserContextProvider = ({ children }: UserContextProviderProps) => {
    const [jwt, setJwt] = useState<string | null>(null);
    return <Context.Provider value={{ jwt, setJwt }} >
        {children}
    </Context.Provider >
}
export default Context;