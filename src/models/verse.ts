import { fieldTypes } from "../config";

const model = {
    name_sing: "versículo",
    name_plur: "versículos",
    fields: [
        { code: "id", name: "Id", type: fieldTypes.textInput },
        { code: "content", name: "Contenido", type: fieldTypes.textArea },
        { code: "number", name: "Número", type: fieldTypes.textInput },
        { code: "Reference", name: "Referencia", type: fieldTypes.textInput },
    ]
};

export default model;