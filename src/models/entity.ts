import { fieldTypes } from "../config";


const model = {
    name_sing: "entidad",
    name_plur: "entidades",
    fields: [
        { code: "id", name: "Id", type: fieldTypes.textInput },
        { code: "entityName", name: "Nombre", type: fieldTypes.textInput },
        { code: "projectId", name: "Proyecto", type: fieldTypes.dropdown },
    ]
};

export default model;