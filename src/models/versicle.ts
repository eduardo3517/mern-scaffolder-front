import { fieldTypes } from "../config";

const model = {
    name_sing: "versículo",
    name_plur: "versículos",
    fields: [
        { code: "id", name: "Id", type: fieldTypes.textInput },
        { code: "book", name: "Libro", type: fieldTypes.dropdown },
        { code: "number", name: "Número", type: fieldTypes.number },
        { code: "rvr1960", name: "Versión RVR1960", type: fieldTypes.textArea },
        { code: "ntv", name: "Versión Nueva Versión Internacional", type: fieldTypes.textArea },
        { code: "lbla", name: "Versión La Biblia De Las Américas", type: fieldTypes.textArea },
    ]
};

export default model;