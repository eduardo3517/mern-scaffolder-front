import { fieldTypes } from "../config";

const model = {
    name_sing: "capítulo",
    name_plur: "capítulos",
    fields: [
        { code: "id", name: "Id", type: fieldTypes.textInput },
        { code: "bookId", name: "Código", type: fieldTypes.textInput },
        { code: "number", name: "Número", type: fieldTypes.dropdown },
        { code: "reference", name: "Referencia", type: fieldTypes.textInput },
    ]
};

export default model;