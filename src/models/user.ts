export class User {
    private name: string;
    private lname: string;
    private email: string;
    private password: string;

    public constructor(email: string, password: string, name?: string, lname?: string) {
        this.name = name || '';
        this.lname = lname || '';
        this.email = email;
        this.password = password;
    }
}