import { fieldTypes } from "../config";

const model = {
    name_sing: "libro",
    name_plur: "libros",
    fields: [
        { code: "id", name: "Id", type: fieldTypes.textInput },
        { code: "abbreviation", name: "Abreviación", type: fieldTypes.textInput },
        { code: "chapters", name: "Capítulos", type: fieldTypes.dropdown },
        { code: "code", name: "Código", type: fieldTypes.textInput },
        { code: "longName", name: "Nombre Largo", type: fieldTypes.textInput },
        { code: "shortName", name: "Nombre", type: fieldTypes.textInput },
        { code: "classification", name: "Clasificación", type: fieldTypes.dropdown },
        { code: "ordering", name: "Orden", type: fieldTypes.textInput },
    ]
};

export default model;