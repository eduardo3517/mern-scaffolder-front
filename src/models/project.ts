import { fieldTypes } from "../config";

const model = {
    name_sing: "proyecto",
    name_plur: "proyectos",
    fields: [
        { code: "id", name: "Id", type: fieldTypes.textInput },
        { code: "projectName", name: "Nombre", type: fieldTypes.textInput },
        { code: "server", name: "Servidor", type: fieldTypes.textInput },
        { code: "db_name", name: "Nombre Base de Datos", type: fieldTypes.textInput },
        { code: "db_user", name: "Usuario de Base de Datos", type: fieldTypes.textInput },
        { code: "pass", name: "Contraseña base de datos", type: fieldTypes.textInput },
    ]
};

export default model;