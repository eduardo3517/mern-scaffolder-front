import axios from "axios";

export default axios.create({
  baseURL: "https://8vl02v06d4.execute-api.us-east-1.amazonaws.com",
  headers: {
    'Content-Type': 'application/json'
  },
});