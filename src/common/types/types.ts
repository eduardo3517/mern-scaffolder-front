export interface IEditProjectProps {
    handleClose: any;
    show: any;
    project: IProject;
}

export interface IEditVersicleProps {
    handleClose: any;
    show: any;
    versicle: IVersicle;
}

export interface IDeleteProjectProps {
    handleClose: any;
    show: any;
    projectId: string;
}
export interface IDeleteVersicleProps {
    handleClose: any;
    show: any;
    versicleId: string;
}

export interface IProject {
    id: string,
    projectName: string,
    server: string,
    db_name: string,
    db_user: string,
    pass: string
}

export interface IBook {
    id: string,
    abbreviation: string,
    chapters: IChapter[],
    code: string,
    longName: string,
    shortName: string,
    classification: string,
    ordering: string
}

export interface IChapter {
    
    id: string,
    bookId: string,
    number: string,
    reference: string,
    verses: IVerse[]
}

export interface IVerse {
    id: string,
    content: string,
    number: string,
    reference: string
}

export interface IVersicle {
    id: string,
    book: string,
    number: string,
    rvr1960: string,
    ntv: string,
    lbla: string
}

export interface IProjects {
    Items: any[]
}

export interface IField {
    code: string, name: string, type: string
}

export interface IEntity {
    id: string,
    entityName: string,
    projectId: string
}

// export interface IUser {
//     name: string;
//     lname: string;
//     email: string;
//     password: string;
// }

export interface INewModalProps {
    handleClose: any;
    show: any;
}

export interface IDeleteEntityProps {
    handleClose: any;
    show: any;
    entityId: string;
}

export interface IDeleteBookProps {
    handleClose: any;
    show: any;
    bookId: string;
}

export interface IEditEntityProps {
    handleClose: any;
    show: any;
    entity: IEntity;
}

export interface IEditBookProps {
    handleClose: any;
    show: any;
    book: IBook;
}


export interface ITooltipProps {
    text: string;
    children: any;
}

export interface IClassification {
    code: string;
    description: string;
}