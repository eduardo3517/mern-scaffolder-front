import React, { useState } from 'react';
import './App.css';
import Navbar from './components/navbar';
import 'bootstrap/dist/css/bootstrap.min.css';
import ProjectComponentList from './components/project/projectsList';
import EntityComponentList from './components/entity/entitiesList';
import { Route, Routes } from 'react-router-dom';
import { MyGlobalContext } from '../src/context/staticContext';
import { UserContextProvider } from '../src/context/userContext';
import { IProject } from './common/types/types';
import Signup from './components/user/signup';
import Login from './components/user/login';
import VersicleComponentList from './components/versicle/versicleList';
import BookComponentList from './components/book/booksList';


function App() {
  const [selectedProject, setSelectedProject] = useState<IProject>({} as unknown as IProject);

  return (
    <UserContextProvider>
      <MyGlobalContext.Provider value={{ selectedProject, setSelectedProject }}>
        <div className="App">
          <Navbar />
          <Routes>
            <Route path='/versicles' element={<VersicleComponentList />} />
            <Route path='/' element={<BookComponentList />} />
            <Route path='/projects' element={<ProjectComponentList />} />
            <Route path='/entities' element={<EntityComponentList />} />
            <Route path='/login' element={<Login />} />
            <Route path='/signup' element={<Signup />} />
          </Routes>
        </div>
      </MyGlobalContext.Provider >
    </UserContextProvider>
  );
}

export default App;
