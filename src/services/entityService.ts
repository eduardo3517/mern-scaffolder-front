import axiosInstance from "../axiosConfig";
import { IEntity } from "../common/types/types";

const getAll = async (): Promise<IEntity[]> => {
    try {
        const response = await axiosInstance.request({
            method: 'GET',
            url: '/entities',
        });
        return response.data.body.entities;
    } catch (error) {
        console.error(error);
        throw error
    }
};

const get = async (id: string): Promise<IEntity> => {
    try {
        const response = await axiosInstance.get(`/entities/${id}`);
        return response.data.body.entities;
    } catch (error: unknown) {
        console.error(error);
        throw error
    }

};

const create = async (data: IEntity): Promise<void> => {
    try {
        await axiosInstance.request({
            method: 'POST',
            url: '/entities/',
            data
        });
    } catch (error) {
        console.error(error);
        throw error
    }

}

const deleteRecord = async (id: string) => {
    try {
        const response = await axiosInstance.delete(`/entities/${id}`);
        return response;
    } catch (error) {
        console.error(error);
        throw error
    }

}

const update = async (id: string, data: IEntity) => {

    try {
        const response = await axiosInstance.request({
            method: 'PUT',
            url: `/entities/${id}`,
            data
        });
        return response;
    } catch (error) {
        console.error(error);
        throw error
    }
}

const EntityService = {
    getAll,
    get,
    create,
    update,
    deleteRecord
};

export default EntityService;