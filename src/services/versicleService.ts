import axiosInstance from "../axiosConfig";
import { IVersicle } from "../common/types/types";

const getAll = async (): Promise<IVersicle[]> => {
    try {
        const response = await axiosInstance.request({
            method: 'GET',
            url: '/versicles',
        });
        return response.data.body.versicles;
    } catch (error) {
        console.error(error);
        throw error
    }
};

const get = async (id: string): Promise<IVersicle> => {
    try {
        const response = await axiosInstance.get(`/versicles/${id}`);
        return response.data.body.versicle;
    } catch (error: unknown) {
        console.error(error);
        throw error
    }

};

const create = async (data: IVersicle): Promise<void> => {
    try {
        await axiosInstance.request({
            method: 'POST',
            url: '/versicles/',
            data
        });
    } catch (error) {
        console.error(error);
        throw error
    }

}

const deleteRecord = async (id: string) => {
    try {
        const response = await axiosInstance.delete(`/versicles/${id}`);
        return response;
    } catch (error) {
        console.error(error);
        throw error
    }

}

const update = async (id: string, data: IVersicle) => {

    try {
        const response = await axiosInstance.request({
            method: 'PUT',
            url: `/versicles/${id}`,
            data
        });
        return response;
    } catch (error) {
        console.error(error);
        throw error
    }
}

const VersicleService = {
    getAll,
    get,
    create,
    update,
    deleteRecord
};

export default VersicleService;