import axiosInstance from "../axiosConfig";
import { User } from "../models/user";

const getAll = async (): Promise<User[]> => {
    try {
        const response = await axiosInstance.request({
            method: 'GET',
            url: '/users',
        });
        return response.data.body.entities;
    } catch (error) {
        console.error(error);
        throw error
    }
};

const get = async (id: string): Promise<User> => {
    try {
        const response = await axiosInstance.get(`/entities/${id}`);
        return response.data.body.entities;
    } catch (error: unknown) {
        console.error(error);
        throw error
    }

};

const create = async (data: User) => {
    try {
        const rpta = await axiosInstance.request({
            method: 'POST',
            url: '/users/signup',
            data
        });

        return rpta.data;
    } catch (error: unknown) {
        console.error(error);
        throw error
    }
}

const login = async (email: string, password: string) => {
    try {
        const rpta = await axiosInstance.request({
            method: 'POST',
            url: '/users/login',
            data: { email, password }
        });
        console.log('login user service', rpta)
        return rpta.data;
    } catch (error: unknown) {
        console.error(error);
        throw error
    }
}


const deleteRecord = async (id: string) => {
    try {
        const response = await axiosInstance.delete(`/entities/${id}`);
        return response;
    } catch (error) {
        console.error(error);
        throw error
    }

}

const update = async (id: string, data: User) => {

    try {
        const response = await axiosInstance.request({
            method: 'PUT',
            url: `/entities/${id}`,
            data
        });
        return response;
    } catch (error) {
        console.error(error);
        throw error
    }
}

const userService = {
    getAll,
    get,
    create,
    update,
    deleteRecord,
    login
};

export default userService;