import axiosInstance from "../axiosConfig";
import { IBook } from "../common/types/types";

const getAll = async (): Promise<IBook[]> => {
    try {
        const response = await axiosInstance.request({
            method: 'GET',
            url: '/books',
        });
        return response.data.body.responseBook;
    } catch (error) {
        console.error(error);
        throw error
    }
};

const get = async (id: string): Promise<IBook> => {
    try {
        const response = await axiosInstance.get(`/books/${id}`);
        return response.data.body.book;
    } catch (error: unknown) {
        console.error(error);
        throw error
    }

};

const create = async (data: IBook): Promise<void> => {
    try {
        await axiosInstance.request({
            method: 'POST',
            url: '/books/',
            data
        });
    } catch (error) {
        console.error(error);
        throw error
    }

}

const deleteRecord = async (id: string) => {
    try {
        const response = await axiosInstance.delete(`/books/${id}`);
        return response;
    } catch (error) {
        console.error(error);
        throw error
    }
}

const importVerses = async (bookId: string, chapterNumber: string) => {
    try {
        const response = await axiosInstance.request({
            method: 'POST',
            url: '/books/',
            data: {bookId, chapterNumber}
        });
        return response;
    } catch (error) {
        console.error(error);
        throw error
    }
}

const update = async (id: string, data: IBook) => {

    try {
        const response = await axiosInstance.request({
            method: 'PUT',
            url: `/books/${id}`,
            data
        });
        return response;
    } catch (error) {
        console.error(error);
        throw error
    }
}

const BookService = {
    getAll,
    get,
    create,
    update,
    deleteRecord,
    importVerses
};

export default BookService;