import axiosInstance from "../axiosConfig";
import { IProject } from "../common/types/types";

const getAll = async (): Promise<IProject[]> => {
    try {
        const response = await axiosInstance.request({
            method: 'GET',
            url: '/projects',
        });
        return response.data.body.projects;
    } catch (error) {
        console.error(error);
        throw error
    }
};

const get = async (id: string): Promise<IProject> => {
    try {
        const response = await axiosInstance.get(`/projects/${id}`);
        return response.data.body.project;
    } catch (error: unknown) {
        console.error(error);
        throw error
    }

};

const create = async (data: IProject): Promise<void> => {
    try {
        await axiosInstance.request({
            method: 'POST',
            url: '/projects/',
            data
        });
    } catch (error) {
        console.error(error);
        throw error
    }

}

const deleteRecord = async (id: string) => {
    try {
        const response = await axiosInstance.delete(`/projects/${id}`);
        return response;
    } catch (error) {
        console.error(error);
        throw error
    }

}

const update = async (id: string, data: IProject) => {

    try {
        const response = await axiosInstance.request({
            method: 'PUT',
            url: `/projects/${id}`,
            data
        });
        return response;
    } catch (error) {
        console.error(error);
        throw error
    }
}

const ProjectService = {
    getAll,
    get,
    create,
    update,
    deleteRecord
};

export default ProjectService;